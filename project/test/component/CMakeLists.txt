set(TARGET_NAME componentTest)

file(GLOB TEST_SOURCES *.cpp)

add_executable(
    ${TARGET_NAME} 
    ${TEST_SOURCES}
    # Files to be tested
    ${PROJECT_SRC_DIR}/Component.cpp
)

target_include_directories(
    ${TARGET_NAME}
    PUBLIC
    SYSTEM
    ${GTEST_INCLUDE_DIR}
    ${GMOCK_INCLUDE_DIR}
)

target_include_directories(
    ${TARGET_NAME}
    PUBLIC
    .
    ${PROJECT_INC_DIR}
)

target_compile_options(${TARGET_NAME} PUBLIC -Wall -Wextra -pedantic)
target_compile_features(${TARGET_NAME} PUBLIC cxx_std_17)

target_link_libraries(
    ${TARGET_NAME}
    PUBLIC
    gmock
    gmock_main
)

add_test(${TARGET_NAME} ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${TARGET_NAME})
add_dependencies(${TARGET_NAME} gmock gmock_main)
add_dependencies(check ${TARGET_NAME})
