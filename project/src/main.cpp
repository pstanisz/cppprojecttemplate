#include <iostream>
#include <cstdlib>

#include <Version.h>
#include <Component.h>

int main()
{
    std::cout << "Cpp Project Template " << VersionMajor << "." << VersionMinor << std::endl;
    std::cout << "With: googletest-release-1.8.0" << std::endl;

    cppprojecttemplate::Component obj;
    auto result = obj.foo();
    result++;   // warning silenced

    return EXIT_SUCCESS;
}
