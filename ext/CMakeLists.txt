# Add googletest subdirectory
add_subdirectory(googletest-release-1.8.0 EXCLUDE_FROM_ALL)

# Set googletest and googlemock include directories for the parent project
set(GTEST_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/googletest-release-1.8.0/googletest/include PARENT_SCOPE)
set(GMOCK_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/googletest-release-1.8.0/googlemock/include PARENT_SCOPE)
